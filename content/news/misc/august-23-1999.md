---
title:    "August 23, 1999"
date:     1999-08-23 00:00:00
category: misc
tags:     []
---
Anonymous CVS access is back online so suck up all the bandwidth you can
afford. Note that this is a new archive, so you will need to re-checkout
the archive. That means don't *cvs update* from a previous download.
