---
title:    "Mesa 20.2.4 is released"
date:     2020-12-04 13:00:51
category: releases
tags:     []
---
[Mesa 20.2.4](https://docs.mesa3d.org/relnotes/20.2.4.html) is released. This is a bug fix release.
