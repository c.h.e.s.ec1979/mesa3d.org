---
title:    "Mesa 20.3.0 is released"
date:     2020-12-03 10:31:38
category: releases
tags:     []
---
[Mesa 20.3.0](https://docs.mesa3d.org/relnotes/20.3.0.html) is released. This is a new development release. See the release notes for more information about this release.
