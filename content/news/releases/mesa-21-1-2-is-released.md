---
title:    "Mesa 21.1.2 is released"
date:     2021-06-02 22:35:56
category: releases
tags:     []
---
[Mesa 21.1.2](https://docs.mesa3d.org/relnotes/21.1.2.html) is released. This is a bug fix release.
